const appRoot = require("app-root-path"),
  winston = require("winston");
require("winston-daily-rotate-file");
require("dotenv").config();

var logLevel = process.env.LOG_LEVEL;

console.log("The log level is : ", logLevel);

var options = {
  file: {
    level: logLevel,
    filename: `${appRoot}/logs/app.log`,
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    colorize: false
  },
  console: {
    level: logLevel,
    handleExceptions: true,
    json: false,
    colorize: true
  }
};

var dailyRotateTransport = new winston.transports.DailyRotateFile({
  filename: "application-%DATE%.log",
  datePattern: "YYYY-MM-DD",
  zippedArchive: true,
  maxSize: "50m",
  maxFiles: "365d",
  dirname: `${appRoot}/logs`
});

logFormat = winston.format.printf(info => {
  const formattedDate = info.timestamp.replace("T", " ").replace("Z", "");
  return `${formattedDate} ${info.level} | ${info.message}`;
});

var logger = winston.createLogger({
  level: logLevel || "info",
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.splat(),
    winston.format.simple()
  ),
  transports: [
    // new winston.transports.File(options.file),
    new winston.transports.Console(options.console),
    dailyRotateTransport
  ],
  exitOnError: false // do not exit on handled exceptions
});

module.exports = logger;

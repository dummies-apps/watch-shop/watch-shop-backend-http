const fs = require("fs"),
  path = require("path");

const getContent = fileName => {
  return JSON.parse(
    fs.readFileSync(
      path.join(
        __dirname,
        "../",
        "services",
        `${fileName}`,
        `/${fileName}.json`
      )
    )
  );
};

module.exports = getContent;

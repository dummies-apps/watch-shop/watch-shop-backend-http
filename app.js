const express = require("express"),
  app = express(),
  cors = require("cors");
winston = require("./config/winston");
require("dotenv").config();

// enable all cors
app.use(cors());

//get application context
var baseURL = process.env.BASE_URL;

app.use(baseURL, require("./services/common/healthCheck"));
app.use(baseURL, require("./services/deals/deals"));
app.use(baseURL, require("./services/gifts/gifts"));

const httpPort = process.env.APPLICATION_PORT || 8080;
app.listen(httpPort, function() {
  winston.debug(
    "Http Server %d is listening on port %d",
    process.pid,
    httpPort
  );
});

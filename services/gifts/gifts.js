const express = require("express"),
  getContent = require("../../utils/jsonFileUtils"),
  router = express(),
  winston = require("../../config/winston");

router.get("/gifts", function(req, res) {
  winston.debug(`Gifts service request`);
  res.status(200).send(getContent("gifts"));
});

module.exports = router;

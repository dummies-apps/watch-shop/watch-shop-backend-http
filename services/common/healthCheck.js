const express = require("express"),
  router = express(),
  winston = require("../../config/winston");

router.get(["/", "/healthz"], function(req, res) {
  winston.debug(`HealthCheck service request`);
  var fullUrl = req.protocol + "://" + req.get("host") + req.originalUrl;
  res.status(200).send({
    status: "UP",
    route: fullUrl
  });
});

module.exports = router;
